<?php
class Beltranc_AdminGrid_Block_Adminhtml_Adgrid_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	
	public function __construct(){
		parent::__construct();

		$this->setDefaultSort('id');
		$this->setId('admingrid_adgrid_grid');
		$this->setDefaultDir('asc');
		$this->setSaveParametersInSession(true);
	}

	public function _getCollectionClass(){
		return 'admingrid/adgrid_collection';
	}

	public function _prepareCollection(){
		$collection = Mage::getResourceModel($this->_getCollectionClass());
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	public function _prepareColumns(){
		$this->addColumn('id',
			array(
				'header'	=> $this->__('ID'), 
				'align'		=> 'right',
				'widht'		=> '50px',
				'index'		=> 'id'
			)
		);

		$this->addColumn('item',
			array(
				'header'	=> $this->__('Items'),
				'index'		=> 'item'
			)
		);
		return parent::_prepareColumns();
	}
}